alias dbg_build="ament build --build-tests --symlink-install --isolated --parallel --cmake-args -DCMAKE_BUILD_TYPE=Debug --"
alias dbg_test="ament test --skip-build --symlink-install --isolated --parallel --cmake-args -DCMAKE_BUILD_TYPE=Debug --"

export LANG=en_US.UTF-8
export ROS_DOMAIN_ID=101
